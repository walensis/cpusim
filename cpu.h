/**********************************************************
** Codes associes aux instructions
***********************************************************/
#define INST_ADD	(0)
#define INST_SUB	(1)
#define INST_CMP	(2)
#define INST_IFGT	(3)
#define INST_NOP	(4)
#define INST_JUMP	(5)
#define INST_HALT	(6)
#define INST_SYSC	(7)
#define INST_LOAD	(8)
#define INST_STORE	(9)


PSW cpu_ADD(PSW m);
PSW cpu_SUB(PSW m);
PSW cpu_CMP(PSW m);
PSW cpu_IFGT(PSW m);
PSW cpu_NOP(PSW m);
PSW cpu_JUMP(PSW m);
void cpu_HALT(void);
PSW cpu_SYSC(PSW m);
PSW cpu_LOAD(PSW m);
PSW cpu_STORE(PSW m);
PSW cpu(PSW m);
