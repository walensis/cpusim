$CFLAGS=
cpu:
	gcc -c -g cpu.c -o cpu.o

sys:
	gcc -c -g sys.c -o sys.o

simul : sys cpu
	gcc -c -g simul.c -o simul.o

all : simul
	gcc -Wall cpu.o sys.o simul.o -o simul

clean:
	rm *.o simul
