#include "simul.h"
#include "cpu.h"
#include "sys.h"
#include <stdio.h>

int ticks_left = TICKS_PER_CLOCK;

/**********************************************************
** Simulation de la CPU (mode utilisateur)
***********************************************************/

/* instruction d'addition */
PSW cpu_ADD(PSW m) {
	m.AC = m.DR[m.RI.i] += (m.DR[m.RI.j] + m.RI.ARG);
	m.PC += 1;
	return m;
}

/* instruction de soustraction */
PSW cpu_SUB(PSW m) {
	m.AC = m.DR[m.RI.i] -= (m.DR[m.RI.j] + m.RI.ARG);
	m.PC += 1;
	return m;
}

/* instruction de comparaison */
PSW cpu_CMP(PSW m) {
	m.AC = (m.DR[m.RI.i] - (m.DR[m.RI.j] + m.RI.ARG));
	m.PC += 1;
	return m;
}

/* instruction "si est plus grand que alors sauter" */
PSW cpu_IFGT(PSW m) {
  //printf("COMPARING AC %d > 0?\n", m.AC);
  if (m.AC > 0)
    m.PC = m.DR[m.RI.i] + m.RI.ARG;
  else
    m.PC += 1;
  return m;
}

/* instruction nulle */
PSW cpu_NOP(PSW m) {
  m.PC += 1;
  return m;
}

/* instruction de saut */
PSW cpu_JUMP(PSW m) {
  m.PC = m.DR[m.RI.i] + m.RI.j + m.RI.ARG;
  
  return m;
}

/* instruction de pause */
void cpu_HALT(void) {
  return;
}

/* instruction d'appel système */
PSW cpu_SYSC(PSW m) {
  //printf(P_OK" Registered syscall %d\n", m.RI.ARG);
  m.IN = INT_SYSC;
  
  return m;
}

/* instruction de chargement dans un registre */
PSW cpu_LOAD(PSW m) {
  m.AC = m.DR[m.RI.j] + m.RI.ARG;
  if (m.AC < 0 || m.AC >= m.SS) {
    m.IN = INT_SEGV;
    return m;
  }
  m.AC = mem[m.SB+m.AC];
  m.DR[m.RI.i] = m.AC;
  m.PC += 1;

  return m;
}

/* instruction de stockage dans la mémoire */
PSW cpu_STORE(PSW m) {
  m.AC = m.DR[m.RI.j] + m.RI.ARG;
  if (m.AC < 0 || m.AC >= m.SS) {
    m.IN = INT_SEGV;
    return m;
  }
  mem[m.SB+m.AC] = m.DR[m.RI.i];
  m.AC = m.DR[m.RI.i];
  m.PC += 1;
  return m;
}

/* Simulation de la CPU */
PSW cpu(PSW m) {
  int i;
  union { WORD word; INST in; } inst;

  while (ticks_left > 0) {
    /*** lecture et decodage de l'instruction ***/
    if (m.PC < 0 || m.PC >= m.SS) {
      m.IN = INT_SEGV;
      return (m);
    }
    inst.word = mem[m.PC + m.SB];
    m.RI = inst.in;
    /*** execution de l'instruction ***/
    switch (m.RI.OP) {
    case INST_ADD:
      m = cpu_ADD(m);
      break;
    case INST_SUB:
      m = cpu_SUB(m);
      break;
    case INST_CMP:
      m = cpu_CMP(m);
      break;
    case INST_IFGT:
      m = cpu_IFGT(m);
      break;
    case INST_NOP:
      m = cpu_NOP(m);
      break;
    case INST_JUMP:
      m = cpu_JUMP(m);
      break;
    case INST_HALT:
      cpu_HALT();
      break;
    case INST_SYSC:
      return (cpu_SYSC(m));
    case INST_LOAD:
      m = cpu_LOAD(m);
      break;
    case INST_STORE:
      m = cpu_STORE(m);
      break;
    default:
      /*** interruption instruction inconnue ***/
      m.IN = INT_INST;
      return m;
    }

    if (m.IN == INT_SEGV)
      return m;

    ticks_left--;
  }

  /*** interruption apres 3 instructions ***/
  m.IN = INT_CLOCK;
  return m;
}
