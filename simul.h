#include <time.h>

/**********************************************************
** definition de la memoire simulee
***********************************************************/

typedef int WORD;  /* un mot est un entier 32 bits  */

WORD mem[128];     /* memoire                       */


/**********************************************************
** Codage d'une instruction (32 bits)
***********************************************************/

typedef struct {
	unsigned OP: 10;  /* code operation (10 bits)  */
	unsigned i:   3;  /* nu 1er registre (3 bits)  */
	unsigned j:   3;  /* nu 2eme registre (3 bits) */
	short    ARG;     /* argument (16 bits)        */
} INST;


/**********************************************************
** Le Mot d'Etat du Processeur (PSW)
***********************************************************/

typedef struct PSW {    /* Processor Status Word */
	WORD PC;        /* Program Counter */
	WORD SB;        /* Segment Base */
	WORD SS;        /* Segment Size */
	WORD IN;        /* Interrupt number */
	WORD DR[8];     /* Data Registers */
	WORD AC;        /* Accumulateur */
	INST RI;        /* Registre instruction */
} PSW;


/**********************************************************
** multi-tasking et processus
***********************************************************/
#define TICKS_PER_CLOCK	(3)
#define MAX_PROCESS	(20)
#define EMPTY	(0)
#define READY	(1)
#define SLEEP	(2)

typedef struct {
  PSW cpu;
  int state;
  time_t wake_time;
} PROC;

extern int current_process;
extern int ticks_left;
extern int last_adr;
extern PROC process[MAX_PROCESS];

void make_inst(unsigned code, unsigned i, unsigned j, short arg);
void make_inst_adr(int adr, unsigned code, unsigned i, unsigned j, short arg);

/**********************************************************
** Codes SGR et couleur
** https://stackoverflow.com/a/3219471/4558937
***********************************************************/

#define COLOR_FG_WHITE	"\x1b[37m"
#define COLOR_BG_RED	"\x1b[41m"
#define COLOR_BG_GREEN	"\x1b[42m"
#define COLOR_BG_YELLOW	"\x1b[44m"
#define COLOR_BG_BLUE	"\x1b[46m"
#define SGR_BOLD	"\x1b[1m"
#define SGR_RESET	"\x1b[0m"
#define P_INF	SGR_BOLD COLOR_BG_BLUE COLOR_FG_WHITE "INF" SGR_RESET
#define P_WARN	SGR_BOLD COLOR_BG_YELLOW COLOR_FG_WHITE "WARN" SGR_RESET
#define P_ERR	SGR_BOLD COLOR_BG_RED COLOR_FG_WHITE "ERR" SGR_RESET
#define P_OK	SGR_BOLD COLOR_BG_GREEN COLOR_FG_WHITE "OK " SGR_RESET
