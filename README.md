Systèmes d'exploitation
================================

TPs 2-3-4 : Simulation d'un CPU multi-tâches et d'un système
--------------------------------------------------------
**Dépôt Bitbucket :** [Lien](https://bitbucket.org/walensis/cpusim.git)

## Auteur
Hamza EL MEKNASSI.

## Synopsis
Nous allons coder une simulation d'un système comportant notamment un CPU. 
Le programme sera principalement axé sur les cycles du CPU, les interruptions et les interractions avec la mémoire centrale.

## Usage
1. Exécuter `make all`
2. Exécuter `simul`
3. Constater le traitement des instructions des processus et des interruptions

## Exécution
Le programme alterne entre l'exécution des instructions des programmes (mode utilisateur) et l'exécution du code système (mode super utilisateur) lorsque nécessaire, typiquement lorsqu'une interruption survient.
Lorsqu'une interruption d'horloge survient en particulier, le CPU cherche un autre processus prêt à l'exécution et lui donne la main. En prenant l'exemple du _TP3 Q2.1_, on peut observer le comportement du CPU sur deux processus qui partagent le même code en mémoire : ce sont donc des threads qui sont traités simultanément.