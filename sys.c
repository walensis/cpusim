#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "simul.h"
#include "sys.h"
#include "cpu.h"

PROC process[MAX_PROCESS];

/**********************************************************
** Demarrage du systeme
***********************************************************/

PSW systeme_init(void) {
	printf("Booting.\n");
	/*** initialisation des processus ***/
	int i;
	for (i=0; i<MAX_PROCESS; i++)
	  process[i].state = EMPTY;
	
	/*** préparation du premier processus ***/
	memset(&process[0].cpu, 0, sizeof(PSW));
	process[0].cpu.PC = 0;
	process[0].cpu.SB = 0;
	process[0].cpu.SS = 20;
	process[0].state = READY;	
	
	/*** creation d'un programme ***/
	make_inst(INST_SYSC, 0, 0, SYSC_GETCHAR);
	make_inst(INST_ADD, 0, 0, 0); //R0 = 0
	make_inst(INST_ADD, 1, 0, 3); //R1 = 3
	make_inst(INST_ADD, 2, 0, 10); //R2 = 10
	make_inst(INST_SUB, 2, 0, 1); //R2--
	make_inst(INST_SYSC, 0, 1, SYSC_SLEEP); //sleep(3)
	make_inst(INST_SYSC, 2, 0, SYSC_PUTI); //print(R2)
	make_inst(INST_CMP, 2, 0, 0);
	make_inst(INST_IFGT, 0, 0, 4); //R2>0?
        make_inst(INST_SYSC, 0, 0, SYSC_EXIT);
	
	current_process = 0;
	return process[current_process].cpu;
}


/**********************************************************
** Traitement d'erreur de segmentation
***********************************************************/

PSW systeme_seg(PSW m) {
  printf(P_ERR" Received INT_SEGV\n");
  printf("Abort.\n");
  exit(EXIT_FAILURE);
}


/**********************************************************
** Traitement d'interruption TRACE
***********************************************************/

PSW systeme_trace(PSW m) {
  m.IN = 0;
  printf(P_OK" Received INT_TRACE\n");
  
  //Construction chaîne de caractères contenant les DR
  char *str_dr03, *str_dr47; //SIZE OF DREGISTERS HARDCODED
  str_dr03 = malloc(4*(sizeof(WORD) + 6));
  str_dr47 = malloc(4*(sizeof(WORD) + 6));
  sprintf(str_dr03, "DR0 %d;\tDR1 %d;\tDR2 %d;\tDR3 %d;",
	  m.DR[0], m.DR[1], m.DR[2], m.DR[3]);
  sprintf(str_dr47, "DR4 %d;\tDR5 %d;\tDR6 %d;\tDR7 %d;",
	  m.DR[4], m.DR[5], m.DR[6], m.DR[7]);
  
  //Affichage
  printf(P_INF" TRACE: PID %d;\tPC %d;\tAC %d\n", current_process, m.PC, m.AC);
  printf(P_INF" TRACE: %s\n", str_dr03);
  printf(P_INF" TRACE: %s\n", str_dr47);
  return m;
}


/**********************************************************
** Traitement d'interruption INST
***********************************************************/

PSW systeme_inst(PSW m) {
  printf(P_ERR" Received INT_INST\n");
  printf("Abort.\n");
  exit(EXIT_FAILURE);
}


/**********************************************************
** Traitement d'interruption horloge
***********************************************************/

PSW systeme_clock(PSW m) {
  printf(P_OK" Received INT_CLOCK\n");
  //m = systeme_trace(m); //DEBUG
  ticks_left = TICKS_PER_CLOCK;
  int i, now = time(NULL);
  
  /*** sauvegarde du processus donnant la main ***/
  process[current_process].cpu = m;

  /*** recherche d'un nouveau processus prêt ***/
  do {
    /*** réveil d'éventuels processus endormis ***/
    now = time(NULL);
    for (i=0; i<MAX_PROCESS; i++) {
      if (process[i].state == SLEEP) {
	if (process[i].wake_time < now)
	  process[i].state = READY;
      }
    }
  
    current_process = (current_process+1) % MAX_PROCESS;
  } while (process[current_process].state != READY);
  
  /*** chargement du processus prenant la main ***/
  m = process[current_process].cpu;
  return m;
}


/**********************************************************
** Traitement d'appel système
***********************************************************/

PSW systeme_sysc(PSW m) {
  m.IN = 0;
  printf(P_OK" Received SYSCALL %d from PID %d\n", m.RI.ARG, current_process);

  switch (m.RI.ARG) {
  case SYSC_EXIT:
    process[current_process].state = EMPTY;
    //printf(P_OK" Process %d state set to %d\n", current_process, process[current_process].state);
    int i, active_p = 0;
    for (i=0; i<MAX_PROCESS; i++) {
      if (process[i].state != EMPTY)
	active_p++;
    }
    if (active_p == 0)
      exit(EXIT_SUCCESS);

    m = systeme_clock(m);
    break;
    
  case SYSC_PUTI:
    printf("%d\n", m.DR[m.RI.i]);
    m.PC += 1;
    break;
    
  case SYSC_NEW_THREAD: ;
    int child_process = current_process;

    /*** recherche d'un emplacement pour le nouveau processus ***/
    do {
      child_process = (child_process+1) % MAX_PROCESS;
    } while (process[child_process].state != EMPTY);

    m.PC += 1;
    /*** données du fils ***/
    m.DR[m.RI.i] = 0;
    m.AC = 0;
    process[child_process].cpu = m;
    process[child_process].state = READY;

    /*** données du père ***/
    m.DR[m.RI.i] = child_process;
    m.AC = child_process;
    break;
    
  case SYSC_SLEEP:
    process[current_process].wake_time = time(NULL) + m.DR[m.RI.i] + m.DR[m.RI.j];
    process[current_process].state = SLEEP;
    m.PC += 1;
    /** donne la main à un autre processus **/
    ticks_left = 0;
    break;

  case SYSC_GETCHAR:
    printf(P_WARN" Syscall GETCHAR not supported\n");
    m.PC += 1;
    break;
    
  default:
    printf(P_ERR" Bad system call (R1 %d, R2 %d, ARG %d)\n",
	   m.RI.i, m.RI.j, m.RI.ARG);
    m.PC += 1;
  } 
  
 return m;
}

/**********************************************************
** Simulation du systeme (mode systeme)
***********************************************************/

PSW systeme(PSW m) {
	switch (m.IN) {
	case INT_INIT:
	  return (systeme_init());
	case INT_SEGV:
	  return (systeme_seg(m));
	case INT_TRACE:
	  return (systeme_trace(m));
	case INT_INST:
	  return (systeme_inst(m));
	case INT_CLOCK:
	  return (systeme_clock(m));
	case INT_SYSC:
	  return (systeme_sysc(m));
	}
	return m;
}
