/**********************************************************
** Codes d'appel système
***********************************************************/

#define SYSC_EXIT	(0)
#define SYSC_PUTI	(1)
#define SYSC_NEW_THREAD	(2)
#define SYSC_SLEEP	(3)
#define SYSC_GETCHAR	(4)


/**********************************************************
** Codes associes aux interruptions
***********************************************************/

#define INT_INIT	(1)
#define INT_SEGV	(2)
#define INT_INST	(3)
#define INT_TRACE	(4)
#define INT_CLOCK	(5)
#define INT_SYSC	(6)


PSW systeme_init(void);
PSW systeme_seg(PSW m);
PSW systeme_trace(PSW m);
PSW systeme_inst(PSW m);
PSW systeme_clock(PSW m);
PSW system_sysc(PSW m);
PSW systeme(PSW m);
